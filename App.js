/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
import {Platform, StyleSheet, Text,TextInput, View,TouchableHighlight,TouchableOpacity,ScrollView,FlatList} from 'react-native';
import Modal from 'react-native-modal'

const instructions = Platform.select({
  ios: 'Press Cmd+R to reload,\n' + 'Cmd+D or shake for dev menu',
  android:
    'Double tap R on your keyboard to reload,\n' +
    'Shake or press menu button for dev menu',
});

export default class App extends Component {
  state={
    statusModal : false,
    fruit:[
      {id:1,name:'apple'},
      {id:2,name:'apple'},
      {id:3,name:'apple'},
      {id:4,name:'apple'},
      {id:5,name:'apple'},
      {id:6,name:'apple'},
    ],
    list:[
      {title: 'Title Text', key: 'item1'},
      {title: 'Title Text', key: 'item1'},
      {title: 'Title Text', key: 'item1'},
      {title: 'Title Text', key: 'item1'},
      {title: 'Title Text', key: 'item1'},
      {title: 'Title Text', key: 'item1'},
      {title: 'Title Text', key: 'item1'},
      {title: 'Title Text', key: 'item1'}
    ]
  }

  onHadleModal(bol){
    this.setState({
      statusModal: bol
    })
  }

  render() {
    return (
      <View style={{justifyContent:'center',alignItems:'center',flex:1}}>
      <Modal 
      animationInTiming={500}
      animationOutTiming={500}
      animationIn={'zoomInDown'}
      animationOut={'zoomOutDown'}
      onSwipe={() => this.setState({ statusModal: false })}
      onBackdropPress={() => this.setState({ statusModal: false })}
      onBackButtonPress={() => this.setState({ statusModal: false })}
      swipeDirection={Platform.OS==='android'? "right" : "up"}
      backdropColor='rgba(0,0,0,0)'
      backdropOpacity={0.5}
      onRequestClose={() => this.setState({ statusModal: false })}
      isVisible={this.state.statusModal}
      onSwipeThreshold={20}
      avoidKeyboard={true}
      >
       
        <View style={{ flex: 1 ,justifyContent:'center',alignItems:'center',}}>
          <View style={{width:'90%',height:200,backgroundColor:'white',padding:20,borderRadius:10}}>

            <FlatList
              ItemSeparatorComponent={({highlighted}) => (
                <View style={[ highlighted && {marginLeft: 0}]} />
              )}
              data={this.state.list}
              renderItem={({item, separators}) => (
                <TouchableHighlight
                  onPress={() => alert(item.title)}
                  onShowUnderlay={separators.highlight}
                  onHideUnderlay={separators.unhighlight}>
                  <View style={{backgroundColor: 'white'}}>
                    <Text>{item.title}</Text>
                  </View>
                </TouchableHighlight>
              )}
            />
            <Text style={{padding:20,backgroundColor:'green'}}
            onPress={()=>this.setState({statusModal:false})}
            >Close</Text>
          </View>
        </View>
      </Modal>
      <TouchableOpacity style={{backgroundColor:'white',padding:15,borderRadius:30,borderColor:'black',borderWidth:1}}
        onPress={()=>this.setState({statusModal:true})}
      >
        <Text style={{color:'black',textAlign:'center'}}>Modal</Text>
      </TouchableOpacity>
      <TextInput 
        placeholder='input here....'
      />
    </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
  modal:
  {
    justifyContent: 'flex-start',
    backgroundColor: 'white',
    marginHorizontal: 0,
    marginBottom: 0,
    marginTop: Platform.OS === 'ios' ? 14 : 0,
    borderBottomLeftRadius: 0,
    borderBottomRightRadius: 0,
    borderTopLeftRadius: 30,
    borderTopRightRadius: 30,
    overflow: 'hidden',
  }
});
